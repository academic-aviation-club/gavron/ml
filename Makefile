python=./venv/bin/python
pip=./venv/bin/pip
gdown=./venv/bin/gdown # For downloading files from Google Drive

weights_url="https://drive.google.com/uc?id=1cewMfusmPjYWbrnuJRuKhPMwRe_b9PaT&export=download"
weights_file="yolov4.weights"

devel: clear requirements git python weights darknet
.PHONY: darknet git weights

clear:
	rm -rf venv

requirements:
	./utils/check_cmds.sh cmake clang git virtualenv 

git: 
	git submodule init 
	git submodule update
	
python: 
	virtualenv venv --python=`which python3`
	test -f $(python)
	test -f $(pip)
	$(pip) install -r requirements.txt
	
weights:
	$(pip) install gdown
	$(gdown) $(weights_url) -O $(weights_file)
	mv $(weights_file) darknet

darknet:
	./utils/compile_darknet.sh
	
	cd darknet; .$(python) darknet.py # Notice that dot xD
	echo If you can see this, everything is working \<3

import os
os.chdir('./darknet')

from darknet import darknet
import time


start = time.time()
# Load the network to memory
darknet.performDetect(initOnly=True)
end = time.time()

print(f'Network init took {end-start:.2f} secs')

images = [
    'eagle.jpg',
    'giraffe.jpg',
    'horses.jpg',
    'dog.jpg',
    'person.jpg',
    'scream.jpg'
]

for image in images*10:
    start = time.time()
    darknet.performDetect(
        imagePath=f"./data/{image}", showImage = False
    )
    end = time.time()
    print(f'Recognition took {end-start:.2f} secs')
#!/bin/bash

# This script assumes that it's being run from the Makefile.
# If you want to run it manually, run it from the repository root:
# 	$ ./utils/compile_darknet.sh


# If running under Jetson
if uname -a | grep tegra; then
	echo ==========================
	echo == COMPILING FOR JETSON ==
	echo ==========================
	echo
	
	cp makefiles/Makefile_Jetson darknet/Makefile
	cd darknet
	PATH=/usr/local/cuda/bin:$PATH make -j4 
	exit $?
fi

# If running under Linux with Nvidia drivers

if uname -a | grep Linux && lsmod | grep nvidia; then
	echo "OLAF SPECJALNE DLA CIEBIE <3 <3"
	echo ==========================================
	echo == COMPILING FOR LINUX WITH GPU SUPPORT ==
	echo ==========================================
	echo

	cp makefiles/Makefile_Linux_Gpu darknet/Makefile
	cd darknet
	PATH=/usr/local/cuda/bin:$PATH make -j4 
	make -j4
	exit $?
fi


# If running under normal Linux
if uname -a | grep Linux; then

	echo =============================================
	echo == COMPILING FOR LINUX WITHOUT GPU SUPPORT ==
	echo =============================================
	echo

	cp makefiles/Makefile_Normal darknet/Makefile
	cd darknet
	make -j4
	exit $?
fi

# If running under MacOS
if uname -a | grep Darwin; then
	echo =========================
	echo == COMPILING FOR MACOS ==
	echo =========================
	echo


	cp makefiles/Makefile_Normal darknet/Makefile
	cd darknet
	# Make fails for some reason on Darwin,
	# But running only ./build.sh does not
	# generate shared libs, both commands
	# need to be run
	make -j8 || ./build.sh
	exit $?
fi

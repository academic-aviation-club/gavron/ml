import os

os.chdir("./darknet")

import argparse
import logging
import shutil
import time
import json

import coloredlogs

from darknet import darknet

coloredlogs.install(level="DEBUG")
logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser(
    description="Recognize images in the INPUT folder"
    "and place the results in the OUTPUT folder"
)
parser.add_argument(
    "--input",
    required=True,
    type=str,
    help="Folder with the images to be recognised",
)

args = parser.parse_args()

start = time.time()
# Load the network to memory
darknet.performDetect(initOnly=True)
end = time.time()

logger.info(f"Network init took {end-start:.2f} secs")

while True:
    time.sleep(1)
    files_to_recognize = os.listdir(args.input)

    
    found_new_images = False
    for file in files_to_recognize:
        # Skip all non-jpg files
        if not file.endswith('.jpg'):
            logger.debug(f"{file} not a .jpg file, skipping")
            continue

        file_full_path = os.path.join(args.input, file)
        # If a file is jpg, but it has already been recognised, skip it
        if os.path.exists(file_full_path + '.json'):
            logger.debug(f"{file} already recognised, skipping")
            continue

        found_new_images = True

        logger.info(f'Attempting to recognize {file_full_path}')
        
        recognition_result = darknet.performDetect(
            imagePath=file_full_path, showImage=False
        )

        logger.debug(recognition_result)

        results_filename = os.path.join(file_full_path + '.json')

        with open(results_filename, 'w') as results_file:
            results_file.write(json.dumps(recognition_result))
        

    if not found_new_images:
        logger.debug("No images found, chilling out for 30 secs")
        time.sleep(30)
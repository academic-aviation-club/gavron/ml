FROM wnt3rmute/archlinux-with-goodies

RUN pacman -Syu clang --noconfirm

RUN mkdir /ml
COPY . /ml
WORKDIR /ml
RUN pip install -r requirements.txt
RUN make